<?php
class Client extends CI_Model {
    
    function __construct(){
        parent::__construct();
        $this->load->library('session');
    }
    
   //Menampilan data

    public function display_record(){
        $query = $this->db->query("select * from client");
        $data = $query->result();

        return $data;
    }

    public function tambah_data($data){
        $query = $this->db->query("INSERT INTO client(no,nama_domain,status,tgl_buat,tgl_exp,penanggung_jawab,alamat_email,no_telp,paket,tagihan,tmpt_domain,keterangan) VALUES ('".$data['no']."','".$data['domain']."','".$data['status']."','".$data['tgl_buat']."','".$data['tgl_exp']."','".$data['pj']."','".$data['email_alamat']."','".$data['telp']."','".$data['paket']."','".$data['tagihan']."','".$data['tmpt_domain']."','".$data['ket']."')");

        $this->db->set($query);
    }

    public function display_edit($data){
        $query = $this->db->query( "select * from client WHERE no = '".$data."'");
        $row = $query->row();

        $edit['no'] = $row->no;
        $edit['domain'] = $row->nama_domain;
        $edit['status'] = $row->status;
        $edit['tgl_buat'] = $row->tgl_buat;
        $edit['tgl_exp'] = $row->tgl_exp;
        $edit['pj'] = $row->penanggung_jawab;
        $edit['alamat_email'] = $row->alamat_email;
        $edit['no_telp'] = $row->no_telp;
        $edit['paket'] = $row->paket;
        $edit['tagihan'] = $row->tagihan;
        $edit['tmpt_domain'] = $row->tmpt_domain;
        $edit['ket'] = $row->keterangan;

        return $edit;
    }

    public function submit_edit($data){
        $query = $this->db->query( "UPDATE client SET nama_domain = '".$data['domain']."', status = '".$data['status']."', tgl_buat = '".$data['tgl_buat']."',tgl_exp = '".$data['tgl_exp']."',penanggung_jawab = '".$data['pj']."', alamat_email = '".$data['email_alamat']."', no_telp = '".$data['telp']."', paket = '".$data['paket']."', tagihan = '".$data['tagihan']."', tmpt_domain ='".$data['tmpt_domain']."', keterangan = '".$data['ket']."' WHERE no ='".$data['no']."'");

        $this->db->set($query);
    }

    public function delete($data){
        $query = $this->db->query( "DELETE  from client WHERE no = '".$data."'");
        $this->db->set($query);
    }
}
?>