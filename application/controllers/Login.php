<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
    
    public function __construct()
	{
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->model('admin/User');
        $this->load->library('session');
    }
    
    public function index() {
        $session = $this->session->all_userdata();
        if(isset($session["username"])){
            redirect("/Home/dashboard" , "refresh");
        }
        $this->load->view('login/login');
    }

    public function submit(){
        $login = $this->User->get_user_data($this->input->post('username'), $this->input->post('password'));
        if($login == "ok"){
            $user = $this->session->userdata('level');
            
            if($user == 1 ){
            redirect("/Home/dashboard" , "refresh");
            }
            else{
            redirect("/Home/dashboard2" , "refresh");
            }
        }
        else{
           $message = "Password Salah";
           echo "<script type='text/javascript'>alert('$message');</script>";
           redirect("/Login/", "refresh");
        }
    }
}

?>