<?php
	class Project extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->helper('url');
			$this->load->model('admin/Client');
			$this->load->model('admin/Projects');
			$this->load->model('admin/Detail_projects');
			$this->load->model('admin/Historyy');
			$this->load->model('admin/Notif');
			$this->load->library('session');
		}

		public function index() {
			$result['title'] = "Project";
			//Notif
			$result['coba3'] = $this->Notif->display_record();
			//Projects
			$result['data'] = $this->Projects->display_record();
			$user['username'] = $this->session->userdata('username');
			

			$this->load->view('templates/navbar',$result);
			$this->load->view('project/list',$result);
			$this->load->view('templates/sidenav',$user);
			$this->load->view('templates/footer');
		}
		public function detail() {

			$result['title'] = "Detail Project";
			//Projects
			$input = $this->uri->segment('3');
			$hasil = $this->Projects->display_edit($input);
			$result['data'] = $hasil;
			//Session
			$result['nama'] = $this->session->userdata('username');
			//Detail Projects
			$table2 = $this->Detail_projects->display_record($input);
			$result['data2'] = $table2;
			$user['username'] = $this->session->userdata('username');

			//Notif
			$result['coba3'] = $this->Notif->display_record();

			$this->load->view('templates/navbar',$result);
			$this->load->view('project/detail',$result);
			$this->load->view('templates/sidenav',$user);
			$this->load->view('templates/footer');
		}

		public function tambah(){
			$result['title'] = "Tambah Project";
			//Notif
			$result['coba3'] = $this->Notif->display_record();
			$result["action"] = "submit";

			$result["data"]["kode"] = "";
			$result["data"]["nama"] = "";
			$result["data"]["start"] = "";
			$result["data"]["status"] = "";
			$result["data"]["deadline"] = "";
			$result["data"]["detail"] = "";
			$result["data"]["progress"] = "";
			$result["data"]["foto"] = "";
			$user['username'] = $this->session->userdata('username');

			$this->load->view('templates/navbar',$result);
			$this->load->view('project/form',$result);
			$this->load->view('templates/sidenav',$user);
			$this->load->view('templates/footer');
		}

		public function submit(){

			$i=rand(1,10000000);
			$namaFile = "foto".$i.".jpg";
			$namaSementara = $_FILES['foto']['tmp_name'];
			$type = $_FILES['foto']['type'];

			// tentukan lokasi file akan dipindahkan
			$dirUpload = "foto/";

			if($namaFile == $namaFile){
				$i=rand(1,10000000);
				$namaFile = "foto".$i.".jpg";
			}
			if($type == 'image/png' || $type == 'image/jpg' || $type == 'image/jpeg' ){
			// pindahkan file
				$input['nama'] = $this->input->post('nama');
				$input['start'] = $this->input->post('start');
				$input['deadline'] = $this->input->post('deadline');
				$input['detail'] = $this->input->post('detail');
				$input['progress'] = $this->input->post('progress');

				$terupload = move_uploaded_file($namaSementara, $dirUpload.$namaFile);
				$input['foto'] = $namaFile;
				$this->Projects->tambah_data($input);
				redirect('/Project/index', 'refresh');
			}
			else{
				echo "Format Tidak Sesuai";
			}
			
		}

		public function edit() {
			$result['title'] = "Edit Project";
			//Notif
			$result['coba3'] = $this->Notif->display_record();

			//Projects
			$input = $this->uri->segment('3');
			$hasil = $this->Projects->display_edit($input);
			$result['data'] = $hasil;
			$result['action'] = "submitedit";
			$user['username'] = $this->session->userdata('username');

			$this->load->view("templates/sidenav",$user);
			$this->load->view("templates/navbar",$result);
			$this->load->view("project/form",$result);
			$this->load->view("templates/footer");
		}

		public function delete() {
			$judul['title'] = "Delete Project";
			$input = $this->uri->segment('3');
			$this->Projects->delete($input);

			//history
			$coba['no'] = $this->uri->segment('3');
			$coba['kondisi'] = "DELETE Project";
			$coba['tanggal_rub'] = date('y-m-d');
			$this->Historyy->tambah_data_proj($coba);
			
			redirect('/Project/index', 'refresh');
		}

		public function submitedit() {

			$i=rand(1,10000000);
			$namaFile = "foto".$i.".jpg";
			$namaSementara = $_FILES['foto']['tmp_name'];
			$type = $_FILES['foto']['type'];

			// tentukan lokasi file akan dipindahkan
			$dirUpload = "foto/";

			if($namaFile == $namaFile){
				$i=rand(1,10000000);
				$namaFile = "foto".$i.".jpg";
			}

			if($type == 'image/png' || $type == 'image/jpg' || $type == 'image/jpeg'){
			// pindahkan file
				$input['kode'] = $this->input->post('kode');
				$input['nama'] = $this->input->post('nama');
				$input ['start'] = $this->input->post('start');
				$input['deadline'] = $this->input->post('deadline');
				$input['detail'] = $this->input->post('detail');
				$input['progress'] = $this->input->post('progress');

				$terupload = move_uploaded_file($namaSementara, $dirUpload.$namaFile);
				$input['foto'] = $namaFile;

				$this->Projects->submit_edit($input);
				redirect('/Project/index', 'refresh');
			}
			else{
				echo "Format Tidak Sesuai";
			}

			
			redirect('/Project/index', 'refresh');
		}

		public function submit_progress(){
			$input['projectId'] = $this->input->post('projectId');
			$input['progress_harian'] = $this->input->post('progress_harian');
			$input['deskripsi_fitur'] = $this->input->post('deskripsi_fitur');
			$input['tanggal_post'] = $this->input->post('tanggal_post');
			ini_set('date.timezone', 'Asia/Jakarta');
			$input['jam_post'] = date('H:i:s');

			$this->Detail_projects->tambah_data($input);

			redirect('Project/index', 'refresh');
		}

		public function delete_progress(){
			$input = $this->uri->segment('3');
			$this->Detail_projects->delete($input);

			redirect('/Project/index', 'refresh');
		}
	}
?>