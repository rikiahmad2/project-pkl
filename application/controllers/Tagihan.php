<?php
	class Tagihan extends CI_Controller {

		public function __construct()
		{
        	parent::__construct();
        	$this->load->library('form_validation');
       	    $this->load->helper('url');
        	$this->load->model('admin/Client');
        	$this->load->model('admin/Historyy');
        	$this->load->model('admin/Notif');
        	$this->load->library('session');
        	$this->load->library('cetak_pdf');
    	}

    	//menampilkan data table
		public function index() {

			$result['data'] = $this->Client->display_record();
			$result['title'] = "Tagihan";
			$user['username'] = $this->session->userdata('username');
            $result['coba3'] = $this->Notif->display_record();

			$this->load->view("templates/sidenav",$user);
			$this->load->view("templates/navbar", $result);
			$this->load->view("tagihan/table", $result);
			$this->load->view("templates/footer");
		}

		//tampilan tambah data baru
		public function tambah(){
			$result["action"] = "submit";
			$result["data"]["no"] = "";
			$result["data"]["domain"] = "";
			$result["data"]["status"] = "";
			$result["data"]["tgl_buat"] = "";
			$result["data"]["tgl_exp"] = "";
			$result["data"]["pj"] = "";
			$result["data"]["alamat_email"] = "";
			$result["data"]["no_telp"] = "";
			$result["data"]["paket"] = "";
			$result["data"]["tagihan"] = "";
			$result["data"]["tmpt_domain"] = "";
			$result["data"]["ket"] = "";
			$result['nonactive'] = "";
			$result['read'] ="";

			$result['title'] = "TambaH";
			$user['username'] = $this->session->userdata('username');
			$result['coba3'] = $this->Notif->display_record();

			$this->load->view("templates/sidenav",$user);
			$this->load->view("templates/navbar", $result);
			$this->load->view("tagihan/validation", $result);
			$this->load->view("templates/footer");

		}

		//tampilan edit
		public function edit(){
			$input = $this->uri->segment('3');
			$hasil = $this->Client->display_edit($input);
			$result['data'] = $hasil;
			$result['nonactive'] = $hasil["status"] == "TIDAK" ? "selected='selected'" : "";
			$result['action'] = "submitedit";
			$result['read'] = "readonly";

			$result['title'] = "Edit";
			$user['username'] = $this->session->userdata('username');
			$result['coba3'] = $this->Notif->display_record();

			$this->load->view("templates/sidenav",$user);
			$this->load->view("templates/navbar", $result);
			$this->load->view("tagihan/validation",$result);
			$this->load->view("templates/footer");
		}

		// START PROSES SUBMIT FORM
		public function submit() {
			$input['no'] = $this->input->post('no');
			$input ['domain'] = $this->input->post('domain');
			$input['status'] = $this->input->post('status');
			$input['tgl_buat'] = $this->input->post('tanggal_buat');
			$input['tgl_exp'] = $this->input->post('tanggal_expire');
			$input['pj'] = $this->input->post('penanggung_jawab');
			$input['email_alamat'] = $this->input->post('email');
			$input['telp'] = $this->input->post('telp');
			$input['paket'] = $this->input->post('paket');
			$input['tagihan'] = $this->input->post('tagihan');
			$input['tmpt_domain'] = $this->input->post('tempat_domain');
			$input['ket'] = $this->input->post('keterangan');

			//history
			$input['kondisi'] = "TAMBAH data client";
			$input['tanggal_rub'] = date('y-m-d');


			$this->Client->tambah_data($input);
			$this->Historyy->tambah_data($input);
			redirect('/Tagihan/index', 'refresh');
		}

		//submit edit form
		public function submitedit(){

			$input['no'] = $this->input->post('no');
			$input ['domain'] = $this->input->post('domain');
			$input['status'] = $this->input->post('status');
			$input['tgl_buat'] = $this->input->post('tanggal_buat');
			$input['tgl_exp'] = $this->input->post('tanggal_expire');
			$input['pj'] = $this->input->post('penanggung_jawab');
			$input['email_alamat'] = $this->input->post('email');
			$input['telp'] = $this->input->post('telp');
			$input['paket'] = $this->input->post('paket');
			$input['tagihan'] = $this->input->post('tagihan');
			$input['tmpt_domain'] = $this->input->post('tempat_domain');
			$input['ket'] = $this->input->post('keterangan');

			$this->Client->submit_edit($input);
			redirect('/Tagihan/index', 'refresh');

		}
		// END PROSES SUBMIT FORM //


		//delete//
		public function delete(){
			$input = $this->uri->segment('3');
			$this->Client->delete($input);

			$coba['no'] = $this->uri->segment('3');
			$coba['kondisi'] = "Data Client DELETE";
			$coba['tanggal_rub'] = date('y-m-d');
			$this->Historyy->tambah_data($coba);

			echo "data telah di delete";
			redirect('/Tagihan/index', 'refresh');
		}

		//menampilkan invoice
		function invoice() { 
			$cek = $this->input->post('bookId');
			$hasil = $this->Client->display_edit($cek);
			$client['data'] = $hasil;
			$client['data']['diskon'] = $this->input->post('diskon');
			$client['data']['dp'] = $this->input->post('dp');


			$this->session->set_flashdata('item',$cek);

			//View PDF
			$this->load->view('tagihan/pdf', $client);

			//history
			$input['no'] = $cek;
			$input['kondisi'] = "PRINT invoice Client";
			$input['tanggal_rub'] = date('y-m-d');
			$this->Historyy->tambah_data($input);

			//Notif
			$this->Notif->delete($cek);


		}


	}
		
?>