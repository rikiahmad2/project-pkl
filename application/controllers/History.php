<?php
	class History extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->helper('url');
			$this->load->model('admin/Client');
			$this->load->model('admin/Projects');
			$this->load->model('admin/Historyy');
			$this->load->model('admin/Notif');
			$this->load->model('admin/Detail_projects');
			$this->load->library('session');
		}

		public function index() {

			$result['title'] = "Tagihan";
			$result['coba'] = $this->Client->display_record();
			$result['data2'] = $this->Historyy->display_record();
			$result['nama'] = $this->session->userdata('username');
			$user['username'] = $this->session->userdata('username');

			//Notif
			$result['coba3'] = $this->Notif->display_record();

			$this->load->view("templates/sidenav",$user);
			$this->load->view("templates/navbar",$result);
			$this->load->view("/history/history",$result);
			$this->load->view("templates/footer");
		}
	}
?>