<?php
	class Home extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->helper('url');
			$this->load->model('admin/User');
			$this->load->model('admin/Client');
			$this->load->model('admin/Projects');
			$this->load->model('admin/Notif');
			$this->load->library('session');
		}

		public function dashboard() {
			$result['title'] = "Dashboard";
			$result['coba'] = $this->Client->display_record();
			$result['coba2'] = $this->Projects->display_record();
			$user['username'] = $this->session->userdata('username');

			$i = 1;
			foreach($result['coba'] as $row)
            {
                  	$date = $row->tgl_exp;
                    $newdate = date('Y-m-d',strtotime($date.'-1 months'));
                    if($newdate == date('Y-m-d')){
                    $this->Notif->tambah_data($row->no,date('Y-m-d'));
                	}
            }

            $result['coba3'] = $this->Notif->display_record();

			$this->load->view('templates/navbar',$result);
			$this->load->view('dashboard/main',$result);
			$this->load->view('templates/sidenav',$user);
			$this->load->view('templates/footer');
		}

		public function dashboard2() {
			$result['title'] = "Dashboard";
			$result['coba'] = $this->Client->display_record();
			$result['coba2'] = $this->Projects->display_record();
			$user['username'] = $this->session->userdata('username');

			$i = 1;
			foreach($result['coba'] as $row)
            {
                  	$date = $row->tgl_exp;
                    $newdate = date('Y-m-d',strtotime($date.'-1 months'));
                    if($newdate == date('Y-m-d')){
                    $this->Notif->tambah_data($row->no,date('Y-m-d'));
                	}
            }

            $result['coba3'] = $this->Notif->display_record();

			$this->load->view('templates/navbar',$result);
			$this->load->view('dashboard/main2',$result);
			$this->load->view('templates/sidenav',$user);
			$this->load->view('templates/footer');
		}

		public function logout(){
			$this->session->sess_destroy();
			redirect("/Login/" , "refresh");
		}
	}
?>