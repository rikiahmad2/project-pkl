<?php
		$logo = base_url("assets/images/cek.png");

		$pdf = new FPDF('P', 'mm','A4');

        $pdf->AddPage();

        $pdf->Image($logo, $pdf->GetX(),$pdf->GetY(),40);
        $pdf->SetFont('Arial','',11);
        $pdf->Text(151,35,'PT.Senopati Indra Technology');

        $pdf->SetFont('Arial','',10);

        $pdf->Text(156,39,'Jalan Antapani Lama No 10 A', 'C');
        $pdf->Text(178,43,'Bandung 40291', 'C');
        $pdf->Text(181,47,'Kec. Antapani', 'C');
        $pdf->Text(167,51,'Phone: 022 8783 1852', 'C');

        $pdf->SetFont('Arial','B',12);
        $pdf -> SetY(60);
        $pdf -> SetX(12);
        $pdf->setFillColor(230,230,230);
        $pdf->Cell(190,20,'',0,0,'L',TRUE);
        $pdf -> SetY(60);
        $pdf -> SetX(12);
        $pdf->MultiCell(190,8,'Invoice #'.$data['no'],0,'L',TRUE);
        $pdf -> SetY(70);
        $pdf -> SetX(12);
        $pdf->SetFont('Arial','',10);
        $pdf->MultiCell(190,8,'Invoice Date : '.$data['tgl_buat'],0,'L',TRUE);
        $pdf -> SetY(76);
        $pdf -> SetX(12);
        $pdf->MultiCell(190,8,'Invoice Due  : '.$data['tgl_exp'],0,'L',TRUE);
        $pdf -> SetY(96);
        $pdf -> SetX(12);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(190,6,'Yth.Bpk/Ibu',0,0,'L',False);
        $pdf -> SetY(101);
        $pdf -> SetX(12);
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(190,6,$data['domain'],0,0,'L',False);
        $pdf -> SetY(106);
        $pdf -> SetX(12);
        $pdf->Cell(190,6,$data['no_telp'],0,0,'L',False);
        $pdf -> SetY(111);
        $pdf -> SetX(12);
        $pdf->Cell(190,6,$data['alamat_email'],0,0,'L',False);
        $pdf -> SetY(131);
        $pdf -> SetX(12);
        $pdf->SetFont('Arial','B',11);
        $pdf->setFillColor(230,230,230);
        $pdf->Cell(120,8,'Description',1,0,'C',true);
        $pdf -> SetY(131);
        $pdf -> SetX(132);
        $pdf->setFillColor(210,210,210);
        $pdf->Cell(70,8,'Total (Rp.)',1,0,'C',TRUE);

        //set kiri
        $pdf->setFillColor(230,230,230);
        $pdf -> SetY(139);
        $pdf -> SetX(12);
        $pdf->SetFont('Arial','',10);
        
        $pdf->Cell(120,8,'Perpanjangan Domain + Hosting : '.$data['domain'],0,1,'L',true);

        //set kiri
        $pdf -> SetY(147);
        $pdf -> SetX(12);
        $pdf->setFillColor(250,250,250);

        $pdf->Cell(120,8,'Paket ( '.$data['paket'].')',0,1,'L',true);

        //set kiri
        $pdf -> SetY(155);
        $pdf -> SetX(12);
        $pdf->setFillColor(230,230,230);

        $pdf->Cell(120,8,'Per Tanggal : '.$data['tgl_buat'].' Sd '.$data['tgl_exp'],0,1,'L',true);

        //set kiri
        $pdf -> SetY(163);
        $pdf -> SetX(12);
        $pdf->SetFont('Arial','B',10);
        $pdf->setFillColor(250,250,250);

        $pdf->Cell(120,8,' Tagihan ',0,1,'C',true);

        //set kiri
        $pdf -> SetY(171);
        $pdf -> SetX(12);
        $pdf->SetFont('Arial','B',10);
        $pdf->setFillColor(230,230,230);

        $pdf->Cell(120,8,' Potongan/Diskon ',0,1,'C',true);

        //set kiri
        $pdf -> SetY(179);
        $pdf -> SetX(12);
        $pdf->SetFont('Arial','B',10);
        $pdf->setFillColor(250,250,250);

        $pdf->Cell(120,8,' PPN/TAX ',0,1,'C',true);

        //set kiri
        $pdf -> SetY(187);
        $pdf -> SetX(12);
        $pdf->SetFont('Arial','B',10);
        $pdf->setFillColor(230,230,230);

        $pdf->Cell(120,8,' Jumlah Tagihan ',0,1,'C',true);

        //set kiri
        $pdf -> SetY(195);
        $pdf -> SetX(12);
        $pdf->SetFont('Arial','B',10);
        $pdf->setFillColor(255,255,0);

        $pdf->Cell(120,8,' DP ',0,1,'C',true);

        //set kiri
        $pdf -> SetY(203);
        $pdf -> SetX(12);
        $pdf->SetFont('Arial','B',10);
        $pdf->setFillColor(230,230,230);

        $pdf->Cell(120,8,' Sisa Tagihan ',0,1,'C',true);

        //set kiri
        $pdf -> SetY(211);
        $pdf -> SetX(12);
        $pdf->setFillColor(230,230,230);

        $pdf->Cell(190,8,' *Terbilang  :  Contoh ',0,1,'L',true);


        //set kiri
        $pdf -> SetY(219);
        $pdf -> SetX(12);
        $pdf->setFillColor(230,230,230);
        $pdf->Cell(120,8,' GRAND TOTAL  ',1,1,'R',true);



        // set kanan
        $pdf -> SetY(139);
        $pdf -> SetX(132);
        $pdf->setFillColor(210,210,210);
        $pdf->setFillColor(240,240,240);
        $pdf->Cell(70,24,'',0,0,'C',TRUE);

        //set kanan
        $pdf -> SetY(163);
        $pdf -> SetX(132);
        $pdf->SetFont('Arial','B',10);
        $pdf->setFillColor(255,255,250);

        $pdf->Cell(70,8,'Rp. '.number_format($data['tagihan'],0,',','.'),0,0,'R',TRUE);

        //set kanan
        $pdf -> SetY(171);
        $pdf -> SetX(132);
        $pdf->SetFont('Arial','B',10);
        $pdf->setFillColor(230,230,230);

        $pdf->Cell(70,8,'Rp. '.number_format($data['diskon'],0,',','.'),0,0,'R',TRUE);

        //set kanan
        $pdf -> SetY(179);
        $pdf -> SetX(132);
        $pdf->SetFont('Arial','B',10);
        $pdf->setFillColor(250,250,250);

        //Pajak
        $ppn = ($data['tagihan'] - $data['diskon'])*0.1;

        $pdf->Cell(70,8,'Rp. '.number_format($ppn,0,',','.'), 0,0,'R',TRUE);

        //set kanan
        $pdf -> SetY(187);
        $pdf -> SetX(132);
        $pdf->SetFont('Arial','B',10);
        $pdf->setFillColor(230,230,230);

        $total_tagihan = $data['tagihan']+$ppn-$data['diskon'];
        $pdf->Cell(70,8,'Rp. '.number_format($total_tagihan,0,',','.'),0,0,'R',TRUE);

        //set kanan
        $pdf -> SetY(195);
        $pdf -> SetX(132);
        $pdf->SetFont('Arial','B',10);
        $pdf->setFillColor(255,255,0);

        $pdf->Cell(70,8,'Rp. '.number_format($data['dp'],0,',','.'),0,0,'R',TRUE);

        //set kanan
        $pdf -> SetY(203);
        $pdf -> SetX(132);
        $pdf->SetFont('Arial','B',10);
        $pdf->setFillColor(230,230,230);


        $sisa_tagihan = $total_tagihan - $data['dp'];

           if($sisa_tagihan <= 0 ){
                $sisa_tagihan = 0;
           }

        $pdf->Cell(70,8,'Rp. '.number_format($sisa_tagihan,0,',','.'),0,0,'R',TRUE);

        //set kanan
        $pdf -> SetY(219);
        $pdf -> SetX(132);
        $pdf->setFillColor(230,230,230);
        $pdf->Cell(70,8,'Rp. '.number_format($sisa_tagihan,0,',','.'),1,1,'R',true);

        //$pdf->Cell(120,8,'Sub Total',0,0,'R',false);
        //$pdf -> SetY(147);
        //$pdf -> SetX(132);
        //$pdf->Cell(70,8,'Rp. '.$data['tagihan'],0,0,'C',false);



         
        $pdf->Output();

 ?>