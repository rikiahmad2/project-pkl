<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Validation Form</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url("assets/"); ?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url("assets/"); ?>dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Client</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Client</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Data Client</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?=base_url();?>Tagihan/<?= $action;?>" method ="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nomor ID (Opsional)</label>
                    <input type="text" name="no" class="form-control" id="exampleInputEmail1" placeholder="Nomor ID" value="<?=$data['no']?>" <?=$read;?>>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Nama Domain</label>
                    <input type="text" name="domain" class="form-control" id="exampleInputPassword1" placeholder="Nama Domain" value="<?=$data['domain']?>">
                  </div>
                  <div class="form-group">
                    <label>Status</label>
                    <select type="text" name="status" class="form-control" placeholder="MASUKAN">
                        <option value="AKTIF">AKTIF</option>
                        <option value="TIDAK" <?= $nonactive; ?>>TIDAK</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Tanggal Buat</label>
                    <input type="date" name="tanggal_buat" class="form-control" id="exampleInputPassword1" placeholder="Tanggal Buat" value="<?=$data['tgl_buat'] ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Tanggal Expire</label>
                    <input type="date" name="tanggal_expire" class="form-control" id="exampleInputEmail1" placeholder="Tanggal Expire" value="<?=$data['tgl_exp'] ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Penanggung Jawab</label>
                    <input type="text" name="penanggung_jawab" class="form-control" id="exampleInputPassword1" placeholder="Penanggung Jawab" value="<?=$data['pj'] ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email / Alamat</label>
                    <input type="text" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email/Alamat" value="<?=$data['alamat_email'] ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Nomor Telepon</label>
                    <input type="number" name="telp" class="form-control" id="exampleInputPassword1" placeholder="Nomor Telepon" value="<?=$data['no_telp'] ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Paket</label>
                    <input type="text" name="paket" class="form-control" id="exampleInputEmail1" placeholder="Paket" value="<?=$data['paket'] ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Tagihan</label>
                    <input type="number" name="tagihan" class="form-control" id="exampleInputPassword1" placeholder="Tagihan" value="<?=$data['tagihan'] ?>" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Tempat Domain</label>
                    <input type="text" name="tempat_domain" class="form-control" id="exampleInputPassword1" placeholder="Tempat Domain" value="<?=$data['tmpt_domain'] ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Keterangan</label>
                    <input type="text" name="keterangan" class="form-control" id="exampleInputPassword1" placeholder="Keterangan" value="<?=$data['ket'] ?>">
                  </div>
                  <div class="form-group mb-0">
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary btn-md col-2 float-right" onclick="myFunction();">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?= base_url("assets/"); ?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url("assets/"); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jquery-validation -->
<script src="<?= base_url("assets/"); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?= base_url("assets/"); ?>/plugins/jquery-validation/additional-methods.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url("assets/"); ?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url("assets/"); ?>dist/js/demo.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
<script type="text/javascript">
  function myFunction(){
    alert("Data Berhasil Ditambah !");
  }
</script>
<!-- overlayScrollbars -->
<script src="<?= base_url("assets/"); ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
</body>
</html>
